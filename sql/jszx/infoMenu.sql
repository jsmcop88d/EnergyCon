-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理', '1', '1', '/jszx/info', 'C', '0', 'jszx:info:view', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '测站管理菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理查询', @parentId, '1',  '#',  'F', '0', 'jszx:info:list',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理新增', @parentId, '2',  '#',  'F', '0', 'jszx:info:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理修改', @parentId, '3',  '#',  'F', '0', 'jszx:info:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理删除', @parentId, '4',  '#',  'F', '0', 'jszx:info:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测站管理导出', @parentId, '5',  '#',  'F', '0', 'jszx:info:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');
