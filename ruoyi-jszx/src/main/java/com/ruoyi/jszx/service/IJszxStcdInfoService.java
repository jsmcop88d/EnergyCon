package com.ruoyi.jszx.service;

import java.util.List;
import com.ruoyi.jszx.domain.JszxStcdInfo;

/**
 * 测站管理Service接口
 * 
 * @author ezio
 * @date 2020-07-10
 */
public interface IJszxStcdInfoService 
{
    /**
     * 查询测站管理
     * 
     * @param id 测站管理ID
     * @return 测站管理
     */
    public JszxStcdInfo selectJszxStcdInfoById(Long id);

    /**
     * 查询测站管理列表
     * 
     * @param jszxStcdInfo 测站管理
     * @return 测站管理集合
     */
    public List<JszxStcdInfo> selectJszxStcdInfoList(JszxStcdInfo jszxStcdInfo);

    /**
     * 新增测站管理
     * 
     * @param jszxStcdInfo 测站管理
     * @return 结果
     */
    public int insertJszxStcdInfo(JszxStcdInfo jszxStcdInfo);

    /**
     * 修改测站管理
     * 
     * @param jszxStcdInfo 测站管理
     * @return 结果
     */
    public int updateJszxStcdInfo(JszxStcdInfo jszxStcdInfo);

    /**
     * 批量删除测站管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJszxStcdInfoByIds(String ids);

    /**
     * 删除测站管理信息
     * 
     * @param id 测站管理ID
     * @return 结果
     */
    public int deleteJszxStcdInfoById(Long id);
}
