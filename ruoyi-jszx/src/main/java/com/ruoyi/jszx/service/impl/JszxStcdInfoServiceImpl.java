package com.ruoyi.jszx.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jszx.mapper.JszxStcdInfoMapper;
import com.ruoyi.jszx.domain.JszxStcdInfo;
import com.ruoyi.jszx.service.IJszxStcdInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 测站管理Service业务层处理
 * 
 * @author ezio
 * @date 2020-07-10
 */
@Service
public class JszxStcdInfoServiceImpl implements IJszxStcdInfoService 
{
    @Autowired
    private JszxStcdInfoMapper jszxStcdInfoMapper;

    /**
     * 查询测站管理
     * 
     * @param id 测站管理ID
     * @return 测站管理
     */
    @Override
    public JszxStcdInfo selectJszxStcdInfoById(Long id)
    {
        return jszxStcdInfoMapper.selectJszxStcdInfoById(id);
    }

    /**
     * 查询测站管理列表
     * 
     * @param jszxStcdInfo 测站管理
     * @return 测站管理
     */
    @Override
    public List<JszxStcdInfo> selectJszxStcdInfoList(JszxStcdInfo jszxStcdInfo)
    {
        return jszxStcdInfoMapper.selectJszxStcdInfoList(jszxStcdInfo);
    }

    /**
     * 新增测站管理
     * 
     * @param jszxStcdInfo 测站管理
     * @return 结果
     */
    @Override
    public int insertJszxStcdInfo(JszxStcdInfo jszxStcdInfo)
    {
        return jszxStcdInfoMapper.insertJszxStcdInfo(jszxStcdInfo);
    }

    /**
     * 修改测站管理
     * 
     * @param jszxStcdInfo 测站管理
     * @return 结果
     */
    @Override
    public int updateJszxStcdInfo(JszxStcdInfo jszxStcdInfo)
    {
        return jszxStcdInfoMapper.updateJszxStcdInfo(jszxStcdInfo);
    }

    /**
     * 删除测站管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJszxStcdInfoByIds(String ids)
    {
        return jszxStcdInfoMapper.deleteJszxStcdInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除测站管理信息
     * 
     * @param id 测站管理ID
     * @return 结果
     */
    @Override
    public int deleteJszxStcdInfoById(Long id)
    {
        return jszxStcdInfoMapper.deleteJszxStcdInfoById(id);
    }
}
