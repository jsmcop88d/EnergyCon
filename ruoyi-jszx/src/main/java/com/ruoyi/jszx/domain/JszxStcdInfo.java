package com.ruoyi.jszx.domain;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;


/**
 * 测站管理对象 jszx_stcd_info
 * 
 * @author ezio
 * @date 2020-07-10
 */
@ApiModel("测站管理")
public class JszxStcdInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 测站id */
    @ApiModelProperty("测站id")
    private Long id;

    /** 测站编码 */
    @ApiModelProperty("测站编码")
    @Excel(name = "测站编码")
    private String stcd;

    /** 测站名称 */
    @ApiModelProperty("测站名称")
    @Excel(name = "测站名称")
    private String stnm;

    /** 所在地 */
    @ApiModelProperty("所在地")
    @Excel(name = "所在地")
    private String loc;

    /** 运行状态 */
    @ApiModelProperty("运行状态")
    @Excel(name = "运行状态")
    private String runCond;

    /** 经度 */
    @ApiModelProperty("经度")
    @Excel(name = "经度")
    private BigDecimal longitude;

    /** 纬度 */
    @ApiModelProperty("纬度")
    @Excel(name = "纬度")
    private BigDecimal latitude;

    /** 修改时间 */
    @ApiModelProperty("修改时间")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhanUpdateTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStcd(String stcd) 
    {
        this.stcd = stcd;
    }

    public String getStcd() 
    {
        return stcd;
    }
    public void setStnm(String stnm) 
    {
        this.stnm = stnm;
    }

    public String getStnm() 
    {
        return stnm;
    }
    public void setLoc(String loc) 
    {
        this.loc = loc;
    }

    public String getLoc() 
    {
        return loc;
    }
    public void setRunCond(String runCond) 
    {
        this.runCond = runCond;
    }

    public String getRunCond() 
    {
        return runCond;
    }
    public void setLongitude(BigDecimal longitude) 
    {
        this.longitude = longitude;
    }

    public BigDecimal getLongitude() 
    {
        return longitude;
    }
    public void setLatitude(BigDecimal latitude) 
    {
        this.latitude = latitude;
    }

    public BigDecimal getLatitude() 
    {
        return latitude;
    }
    public void setZhanUpdateTime(Date zhanUpdateTime) 
    {
        this.zhanUpdateTime = zhanUpdateTime;
    }

    public Date getZhanUpdateTime() 
    {
        return zhanUpdateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("stcd", getStcd())
            .append("stnm", getStnm())
            .append("loc", getLoc())
            .append("runCond", getRunCond())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("zhanUpdateTime", getZhanUpdateTime())
            .toString();
    }
}
