package com.ruoyi.quartz.task;

import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 *
 * @author ezio
 */
@Component("jszxTask")
public class JszxTask {

    public void jszxDemo()
    {
        System.out.println("也就是测试，喵喵喵");
    }
}
