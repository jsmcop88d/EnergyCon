package com.ruoyi.web.controller.jszx;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.jszx.domain.JszxStcdInfo;
import com.ruoyi.jszx.service.IJszxStcdInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 测站管理Controller
 * 
 * @author ezio
 * @date 2020-07-10
 */
@Api("参数配置")
@Controller
@RequestMapping("/jszx/xtgl/stcdInfo")
public class JszxStcdInfoController extends BaseController
{
    private String prefix = "jszx/xtgl/stcdInfo";

    @Autowired
    private IJszxStcdInfoService jszxStcdInfoService;

    @RequiresPermissions("jszx/xtgl:stcdInfo:view")
    @GetMapping()
    public String stcdInfo()
    {
        return prefix + "/stcdInfo";
    }

    /**
     * 查询测站管理列表
     */
    @ApiOperation("查询测站管理列表")
    @RequiresPermissions("jszx/xtgl:stcdInfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(JszxStcdInfo jszxStcdInfo)
    {
        startPage();
        List<JszxStcdInfo> list = jszxStcdInfoService.selectJszxStcdInfoList(jszxStcdInfo);
        return getDataTable(list);
    }

    /**
     * 导出测站管理列表
     */
    @ApiOperation("导出测站管理列表")
    @RequiresPermissions("jszx/xtgl:stcdInfo:export")
    @Log(title = "测站管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(JszxStcdInfo jszxStcdInfo)
    {
        List<JszxStcdInfo> list = jszxStcdInfoService.selectJszxStcdInfoList(jszxStcdInfo);
        ExcelUtil<JszxStcdInfo> util = new ExcelUtil<JszxStcdInfo>(JszxStcdInfo.class);
        return util.exportExcel(list, "stcdInfo");
    }

    /**
     * 新增测站管理
     */
    @ApiOperation("新增测站管理")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存测站管理
     */
    @ApiOperation("新增保存测站管理")
    @RequiresPermissions("jszx/xtgl:stcdInfo:add")
    @Log(title = "测站管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(JszxStcdInfo jszxStcdInfo)
    {
        return toAjax(jszxStcdInfoService.insertJszxStcdInfo(jszxStcdInfo));
    }

    /**
     * 修改测站管理
     */
    @ApiOperation("修改测站管理")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        JszxStcdInfo jszxStcdInfo = jszxStcdInfoService.selectJszxStcdInfoById(id);
        mmap.put("jszxStcdInfo", jszxStcdInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存测站管理
     */
    @ApiOperation("修改保存测站管理")
    @RequiresPermissions("jszx/xtgl:stcdInfo:edit")
    @Log(title = "测站管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JszxStcdInfo jszxStcdInfo)
    {
        return toAjax(jszxStcdInfoService.updateJszxStcdInfo(jszxStcdInfo));
    }

    /**
     * 删除测站管理
     */
    @ApiOperation("删除测站管理")
    @RequiresPermissions("jszx/xtgl:stcdInfo:remove")
    @Log(title = "测站管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(jszxStcdInfoService.deleteJszxStcdInfoByIds(ids));
    }
}
