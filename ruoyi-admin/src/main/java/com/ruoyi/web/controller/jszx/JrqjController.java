package com.ruoyi.web.controller.jszx;

import com.ruoyi.common.core.controller.BaseController;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 今日全景Controller
 * 
 * @author ezio
 * @date 2020-07-15
 */
@Api("参数配置")
@Controller
@RequestMapping("/jszx/jrqj")
public class JrqjController extends BaseController
{
    private String prefix = "jszx/jrqj";

    @RequiresPermissions("jszx/jrqj:jrqj:view")
    @GetMapping()
    public String jrqj()
    {
        return prefix + "/jrqj";
    }


}
